import { Component, OnInit } from '@angular/core';
import { IProduct } from './product';
import { ProductService } from './product.service';


@Component({
  selector: 'pm-products',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']

})

export class ProductListComponent implements OnInit {

  pageTitle: string = "ACME Product List";
  imageCheck: boolean = false;
  imageWidth: number = 50;
  imageMargin: number = 2;
  _listFilter: string;
  ratingText: string;

  constructor(private productService: ProductService) {
    this.listFilter = "";
  }

  get listFilter(): string {
    return this._listFilter;
  }

  set listFilter(value: string) {
    this._listFilter = value;
    this.filteredProducts = this.listFilter ? this.performFilter(this.listFilter) : this.products;
  }

  showImage() {
    this.imageCheck = !this.imageCheck;
  }

  onRatingClicked(message: string) {
    this.ratingText = message;
  }

  performFilter(filterBy: string): IProduct[] {
    filterBy = filterBy.toLocaleLowerCase();
    return this.products.filter((product: IProduct) =>
      product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }
  ngOnInit(): void {
    console.log("In OnInit");
    this.products = this.productService.getProducts();
    this.filteredProducts = this.products;
  }

  filteredProducts: IProduct[];
  products: IProduct[] = [];

}